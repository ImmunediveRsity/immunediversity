#!/usr/bin/env perl

#  igparse.pl
#  
#  Copyright 2013
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

@VDJ = ();
$i = 0;
open (IGBLAST, "igblast.out") || die "Can't open file.";
open (PARSED, '>parsedigblast');

$/ = "# IGBLASTN 2.2.27+";
while (<IGBLAST>){
        if ( $_ =~ /# Query: (\S+)/){
                $temp = $1."\t";
                if ( $_ =~ /\n(.+)\t(.+)\t(.+)\t(.+)\t(.+)\t-\n/ ){
                        $temp .= $1."\t".$2."\t".$3."\t".$4."\t".$5."\t";
                }
                else{
                        if ( $_ =~ /\n(.+)\t(.+)\t(.+)\t(.+)\t-\n/ ){
                                $temp .= $1."\t\t".$2."\t".$3."\t".$4."\t";
                        }
                        else{
                                $temp .= "\t\t\t\t\t";
                        }
                }
                 if ( $_ =~ /\n(\S+)+\t(\S+)\t(\S+)\t(\S+)\t(\S+)+\t\n/ ){
                        $temp .= $1."\t".$2."\t".$3."\t".$4."\t".$5."\t";
                }
                else{
                        $temp .= "\t\t\t\t\t";
                }
                if ( $_ =~ /V\t\S+\t(\S+)\t\S+\t\S+\t\S+\t\S+\t(\S+)\t(\S+)\t\S+\t\S+\t\S+\s+\S+\n/ ){
                        $temp .= $1."\t".$2."\t".$3."\t";
                }
                else{
                        $temp .= "\t\t\t";
                }
                if ( $_ =~ /D\t\S+\t(\S+)\t\S+\t\S+\t\S+\t\S+\t(\S+)\t(\S+)\t\S+\t\S+\s+\S+\s+\S+\n/ ){
                        $temp .= $1."\t".$2."\t".$3."\t";
                }
                else{
                        $temp .= "\t\t\t";
                }
                if ( $_ =~ /J\t\S+\t(\S+)\t\S+\t\S+\t\S+\t\S+\t(\S+)\t(\S+)\t\S+\t\S+\s+\S+\s+\S+\n/ ){
                        $temp .= $1."\t".$2."\t".$3."\n";
                }
                else{
                        $temp .= "\t\t\n";
                }
                print PARSED $temp;
                $i++;
        }
}
close (IGBLAST); 
close (PARSED);

