#!/bin/bash

#  install.sh
#  
#  Copyright 2013-2014 Andres Aguilar <andresyoshimar@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

###################################################################
#: Title          : install.sh
#: Date           : 03-01-2014
#: Author         : Andres Aguilar <andresyoshimar@gmail.com>
#: Version        : 0.2.4
#: Description    : ImmunediveRsity installer
#: Options        : none
###################################################################

title='ImmunediveRsity'
subtitle='Evaluation of the antibody repertoire by analyzing HTS data'
ast='*****************************************************************'
deps=1

DEPS=0
WDIR=$(pwd)

verify_system() {
    systemName=$(uname)
    
    if [ "$systemName" = "Darwin" ]; then
	systemName="MacOS"
    elif [ "$systemName" = "Linux" ]; then
	systemName="Linux"
    else
	systemName="Unkown"
    fi

    echo "$systemName"
}


log_msg() {
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    NORMAL=$(tput sgr0)

    MSG="$1"
    STATUS="$2"

    if [ "$STATUS" == 0 ] ;  then
	STATUSCOLOR="$GREEN[OK]$NORMAL"
	STATUS2="[OK]"
    else
	STATUSCOLOR="$RED[ERROR]$NORMAL"
	STATUS2="[ERROR]"
    fi

    let COL=$(tput cols)-${#MSG}+${#STATUSCOLOR}-${#STATUS2}

    echo -n $MSG
    printf "%${COL}s\n"  "$STATUSCOLOR"
}

echo_c() {
    w=$(stty size | cut -d" " -f2)
    l=${#1}
               
    printf "%"$((l+(w-l)/2))"s\n" "$1"
}

remove_files() {
    echo -e "\n\n\nABORTING....."
    echo "Removing files"

    # remove ImmunediveRsity directory
    if [ -d /opt/ImmunediveRsity ] ; then
	sudo rm -fr /opt/ImmunediveRsity 
    fi
    
    # remove symbolic links
    if [ -L /usr/bin/ImmunediveRsity ] ; then
	sudo rm /usr/bin/ImmunediveRsity
    fi

    if [ -L /usr/bin/ImmunediveRsity_notifier ] ; then
	sudo rm /usr/bin/ImmunediveRsity_notifier
    fi

    if [ -L /usr/bin/blastall ] ; then
	sudo rm /usr/bin/blastall 
    fi

    echo "DONE"

    exit 1
}

verify_error() {
    msg="$1"
    error="$2"

    if [ "$error" == 1 ] ; then
	log_msg "Error: $msg" "$error"
	remove_files
    else
	log_msg "$msg" "$error"
    fi
}

wellcome() {
    clear

    echo_c "$ast"
    echo_c "$title"
    echo_c "$subtitle"
    echo_c "$ast"
}

verify_prev_installation() {
    # verifying if immunediversity dir exist or if ImmunediveRsity and ImmunediveRsity_notifier exist
    echo "Verify prev installation..."
    
    if [ -d /opt/ImmunediveRsity ] ; then
	log_msg "ImmunediveRsity" 1
	echo "Immunodiversity is already installed, please uninstall to continue..."

	exit 1
    else
	log_msg "ImmunediveRsity" 0
    fi
}

verify_dependencies() {
    # verify if all dependencies are already installed
    echo -e '\nVerifying dependencies...\n'

    error1=0

    # search python
    python=$(whereis python)
    if [ "$python" == "" ] ; then
	log_msg 'Python' 1
	echo -e '\nPython is not installed\nAborting...'
	exit 1
    else
	log_msg 'Python' 0
    fi
    
    # checking Igraph
    python $WDIR/installer/check_igraph.py
    error=$?

    if [ "$error" == 0 ]; then
	log_msg 'iGraph(python)' 0
    else
	log_msg 'iGraph(python)' 1
	echo -e "\npython-igraph is not installed!\n"
	echo "Network graphs will not generated"
    fi

    # checking psutil
    python $WDIR/installer/check_psutil.py
    error=$?

    if [ "$error" == 0 ]; then
	log_msg 'psutil(python)' 0
    else
	log_msg 'psutil(python)' 1
	echo -e "\npython-psutil is not installed!\nAborting..."
	exit 1
    fi


    # search R
    rbin=$(whereis R)
    if [ "$rbin" == "" ] ; then
	log_msg "R" 1
	echo -e '\nR is not installed\nAborting...'
	exit 1
    else    
	if [ "$sys" == "MacOS" ] ; then
	    log_msg 'R' 0
	fi
    fi

    # checking R libraries
    R -q -e "source('installer/dependency_checker.R')" || ! echo 'Error: R dependencies not found!'
    error=$?
	    
    if [ "$error" == 0 ] ; then
	log_msg "R libraries" 0
	deps=0
    else
	log_msg "R libraries" 1
	echo -e "\nR libraries are not installed!\n"
	echo -e "Will be installed later."
	DEPS=1
	#sudo R -q -e "source('installer/install_deps.R')"
    fi

    # Search hmmer
    hmmer=$(whereis hmmsearch)
    # hmmer in MacOS - /usr/local/bin/hmmsearch by default
    sys=$(verify_system)

    if [ "$sys" == "MacOS" ] ; then 
	if [ -x /usr/local/bin/hmmsearch ] ; then 
	    hmmer="exist"
	fi
    fi

    if [ "$hmmer" == "" ] ; then
	log_msg "HMMR" 1
	echo -e "\nHMMR is not installed!\nAborting..."
	exit 1
    else
	log_msg  "HMMR" 0
    fi
}

install() {
    echo 'INTRO TO CONTINUE...'
    read
 
    ## root access
    case $y in
	etch )
	    echo 'root password (su)'
	    su
	    echo 'Starting installation process' 
	    ;;
	* )
	    echo 'root password (sudo)'
	    sudo echo 'Starting installation process'
	    ;;
    esac
    
    echo ""
    echo ""
    
    case $y in 
	etch )

	    ################
	    # install deps #
	    ################

	    if [ "$DEPS" -eq 1 ]; then
	    	R -q -e "source('installer/install_deps.R')"
	    fi

	    # Create ImmunediveRsity directory
	    mkdir /opt/ImmunediveRsity 
	    verify_error "Creating ImmunediveRsity directory" "$?"
	    
	    ################
	    # moving files #
	    ################

	    cp uninstall.sh /opt/ImmunediveRsity/.
	    verify_error "Moving uninstall.sh" "$?"
	    
	    mkdir /opt/ImmunediveRsity/uninstaller
	    verify_error "Creating /opt/ImmunediveRsity/uninstaller" "$?"

	    cp uninstaller/* /opt/ImmunediveRsity/uninstaller/.
	    verify_error "Moving uninstaller scripts" "$?"

 	    cp COPYING /opt/ImmunediveRsity/.
	    verify_error "Moving COPYING to /opt/ImmunediveRsity" "$?"

	    cp README.md /opt/ImmunediveRsity/.
	    verify_error "Moving README.md to /opt/ImmunediveRsity" "$?"

	    # create bin directory
	    mkdir /opt/ImmunediveRsity/bin 
	    verify_error "Creating ImmunediveRsity/bin directory" "$?"

	    cp bin/ImmunediveRsity /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ImmunediveRsity" "$?"
	    
	    cp bin/ImmunediveRsity_notifier /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ImmunediveRsity_notifier" "$?"

	    cp bin/acacia-1.52.b0.jar /opt/ImmunediveRsity/bin/.
	    verify_error "Moving /bin/acacia" "$?"

	    cp bin/cutVDJ.pl /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/cutVDJ.pl" "$?"

	    cp bin/parseKaks /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/parseKaks" "$?"

	    cp bin/network_graph /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/network_graph" "$?"

	    cp bin/complete_table /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/complete_table" "$?"

	    cp bin/div_table /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/div_table" "$?"

	    cp bin/usearc* /opt/ImmunediveRsity/bin/usearch
	    verify_error "Moving bin/usearch" "$?"

	    # SUBSETS
	    cp bin/ImmunediveRsity_subset /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ImmunediveRsity_subset" "$?"

	    cp bin/parseKaks_subset /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/parseKaks_subset" "$?"

	    cp bin/network_graph_subsets /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/network_graph_subset" "$?"

	    cp bin/ng_index /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ng_index" "$?"

	    # Creating bin/blast directories
	    mkdir /opt/ImmunediveRsity/bin/blast-2.2.22 
	    verify_error "Creating blast-2.2.22 directory" "$?"

	    mkdir /opt/ImmunediveRsity/bin/blast-2.2.22/bin
	    verify_error "Creating bin/blast-2.2.22/bin directory" "$?"

	    mkdir /opt/ImmunediveRsity/bin/blast-2.2.22/data 
	    verify_error "Creating bin/blast-2.2.22/data directory" "$?"

	    cp bin/blast-2.2.22/bin/* /opt/ImmunediveRsity/bin/blast-2.2.22/bin/.
	    verify_error "Moving blast binaries" "$?"

	    cp bin/blast-2.2.22/data/* /opt/ImmunediveRsity/bin/blast-2.2.22/data/.
	    verify_error "Moving blast data" "$?"

	    # creating ImmunediveRsity/data directory
	    mkdir /opt/ImmunediveRsity/data
	    verify_error "Creating ImmunediveRsity/data" "$?"

	    cp data/CDR3_IGHV_human.hmm /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/CDR3_IGHV_human.hmm" "$?"

	    cp data/CDR3_mouse.hmm /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/CDR3_IGHV_mouse.hmm" "$?"

	    cp data/blastfirsthit_q.pl /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/blastfirtshit_q.pl" "$?"

	    cp data/gene_coordinates.txt /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/gene_coordinates.txt" "$?"

	    # Creating data/LocusIgH_db
	    mkdir /opt/ImmunediveRsity/data/LocusIgH_db
	    verify_error "Creating data/LocusIgH_db" "$?"

	    cp data/LocusIgH_db/* /opt/ImmunediveRsity/data/LocusIgH_db/.
	    verify_error "Moving data/LocusIgH_db files" "$?"

	    # creating data/IGBLAST
	    mkdir /opt/ImmunediveRsity/data/IGBLAST
	    verify_error "Creating data/IGBLAST directory" "$?"

	    cp data/IGBLAST/ig* /opt/ImmunediveRsity/data/IGBLAST/.
	    verify_error "Moving igblast binaries" "$?"
	    
	    cp data/IGBLAST/myseq /opt/ImmunediveRsity/data/IGBLAST/.
	    verify_error "moving IGBLAST/myseq" "$?"

	    # creating data/IGBLAST/database
	    mkdir /opt/ImmunediveRsity/data/IGBLAST/database
	    verify_error "Creating data/IGBLAST/database" "$?"

	    cp data/IGBLAST/database/* /opt/ImmunediveRsity/data/IGBLAST/database/.
	    verify_error "Moving IGBLAST/database" "$?"

	    # creating data/IGBLAST/functional_db
	    mkdir /opt/ImmunediveRsity/data/IGBLAST/functional_db 
	    verify_error "Creating data/IGBLAST/functional_db" "$?"

	    cp data/IGBLAST/functional_db/* /opt/ImmunediveRsity/data/IGBLAST/functional_db/.
	    verify_error "Moving IGBLAST/functional_db" "$?"

	    # creating IGBLAST/internal_data
	    mkdir /opt/ImmunediveRsity/data/IGBLAST/internal_data
	    verify_error "Creating IGBLAST/internal_data" "$?"
	    
	    mkdir /opt/ImmunediveRsity/data/IGBLAST/internal_data/human
	    verify_error "Creating IGBLAST/internal_data/human" "$?"

	    cp data/IGBLAST/internal_data/human/* /opt/ImmunediveRsity/data/IGBLAST/internal_data/human/.
	    verify_error "Moving internal_data/human" "$?"

	    mkdir /opt/ImmunediveRsity/data/IGBLAST/internal_data/mouse
	    verify_error "Creating internal_data" "$?"

	    cp data/IGBLAST/internal_data/mouse/* /opt/ImmunediveRsity/data/IGBLAST/internal_data/mouse/.
	    verify_error "Moving internal_data/mouse" "$?"

	    # creating IGBLAST/optional_file
	    mkdir /opt/ImmunediveRsity/data/IGBLAST/optional_file
	    verify_error "Creating IGBLAST/optional_file" "$?"

	    cp data/IGBLAST/optional_file/* /opt/ImmunediveRsity/data/IGBLAST/optional_file/.
	    verify_error "Moving IGBLAST/optional_file" "$?"

	    # Creating ImmunediveRsity/ImmunediveRsity directories
	    mkdir /opt/ImmunediveRsity/ImmunediveRsity
	    verify_error "Creating ImmunediveRsity/ImmunediveRsity" "$?"

	    cp ImmunediveRsity/DESCRIPTION /opt/ImmunediveRsity/ImmunediveRsity/.
	    verify_error "Moving ImmunediveRsity/DESCRIPTION" "$?"

	    cp ImmunediveRsity/NAMESPACE /opt/ImmunediveRsity/ImmunediveRsity/.
	    verify_error "Moving ImmunediveRsity/NAMESPACE" "$?"

	    # Creating ImmunediveRsity R directory
	    mkdir /opt/ImmunediveRsity/ImmunediveRsity/R 
	    verify_error "Creating ImmunediveRsity/R directory" "$?"

	    cp ImmunediveRsity/R/* /opt/ImmunediveRsity/ImmunediveRsity/R/.
	    verify_error "Moving R libraries" "$?"
	    
	    # Creating ImmunediveRsity/extdata
	    mkdir /opt/ImmunediveRsity/ImmunediveRsity/extdata
	    verify_error "Creating ImmunediveRsity/extdata" "$?"
	    
	    cp ImmunediveRsity/extdata/* /opt/ImmunediveRsity/ImmunediveRsity/extdata
	    verify_error "Moving ImmunediveRsity/extdata" "$?"

	    # Creating ImmunediveRsity/extras
	    mkdir /opt/ImmunediveRsity/ImmunediveRsity/extras
	    verify_error "Creating ImmunediveRsity/extras" "$?"

	    cp ImmunediveRsity/extras/* /opt/ImmunediveRsity/ImmunediveRsity/extras/.
	    verify_error "Moving ImmunediveRsity/extras" "$?"

	    # Creating ImmunediveRsity/man
	    mkdir /opt/ImmunediveRsity/ImmunediveRsity/man
	    verify_error "Creating ImmunediveRsity/man" "$?"

	    cp ImmunediveRsity/man/* /opt/ImmunediveRsity/ImmunediveRsity/man/.
	    verify_error "Moving ImmunediveRsity/man" "$?"

	    # Goto ImmunediveRsity directory
	    cd /opt/ImmunediveRsity

	    # Add executable permission
	    chmod +x /opt/ImmunediveRsity/bin/ImmunediveRsity*
	    verify_error "Setting executable permission to ImmunediveRsity" "$?"

	    chmod +x /opt/ImmunediveRsity/bin/*
	    verify_error "Setting executable permission to ImmunediveRsity/bin" "$?"

	    chmod +x /opt/ImmunediveRsity/bin/blast-2.2.22/bin/*
	    verify_error "Setting executable permission to blast-2.2.22" "$?"

	    chmod +x /opt/ImmunediveRsity/data/IGBLAST/igblastn 
	    verify_error "Setting executable permission to igblastn" "$?"

	    chmod +x /opt/ImmunediveRsity/data/IGBLAST/igblastp 
	    verify_error "Setting executable permission to igblastp" "$?"
	    
	    ########################
	    # adding files to path #
	    ########################

	    # creating symlinks to /usr/bin
	    ln -s /opt/ImmunediveRsity/bin/ImmunediveRsity /usr/bin/ImmunediveRsity
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity" "$?"

	    ln -s /opt/ImmunediveRsity/bin/ImmunediveRsity_notifier /usr/bin/ImmunediveRsity_notifier
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity_notifier" "$?"

	    ln -s /opt/ImmunediveRsity/bin/ImmunediveRsity_subset /usr/bin/ImmunediveRsity_subset
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity_subset" "$?"

	    ln -s /opt/ImmunediveRsity/bin/blast-2.2.22/bin/blastall /usr/bin/blastall
	    verify_error "Creating symbolic link to /usr/bin/blastall" "$?" 

	    export PATH
	    
	    ###############
	    # configuring #
	    ###############

	    sys=$(verify_system)

	    if [ "$sys" = "Linux" ]; then
		sed -i "s+\$/ = \"\# IGBLASTN 2.2.27\+\";+\$/ = \"\# IGBLASTN 2.2.26\+\";+g" /opt/ImmunediveRsity/data/IGBLAST/igparse.pl
		verify_error "Configuring igparse.pl" "$?"

		sed -i "s/-auxiliary_data/-auxilary_data/g" /opt/ImmunediveRsity/ImmunediveRsity/R/runigblast.R
		verify_error "Configuring runigblast" "$?"
	    fi
	    
	    ########################
	    # Installing R library #
	    ########################

	    echo ""
	    R CMD INSTALL ./ImmunediveRsity
	    echo ""
	    verify_error "Installing R libraries" "$?"
	    ;;
	* )
	    ################
	    # install deps #
	    ################

	    if [ "$DEPS" -eq 1 ]; then
	    	sudo R -q -e "source('installer/install_deps.R')"
	    fi

	    # Create ImmunediveRsity directory
	    sudo mkdir /opt/ImmunediveRsity 
	    verify_error "Creating ImmunediveRsity directory" "$?"

	    ################
	    # moving files #
	    ################

	    sudo cp uninstall.sh /opt/ImmunediveRsity/.
	    verify_error "Moving uninstall.sh" "$?"
	    
	    sudo mkdir /opt/ImmunediveRsity/uninstaller
	    verify_error "Creating /opt/ImmunediveRsity/uninstaller" "$?"

	    sudo cp uninstaller/* /opt/ImmunediveRsity/uninstaller/.
	    verify_error "Moving uninstaller scripts" "$?"

 	    sudo cp COPYING /opt/ImmunediveRsity/.
	    verify_error "Moving COPYING to /opt/ImmunediveRsity" "$?"

	    sudo cp README.md /opt/ImmunediveRsity/.
	    verify_error "Moving README.md to /opt/ImmunediveRsity" "$?"

	    # create bin directory
	    sudo mkdir /opt/ImmunediveRsity/bin 
	    verify_error "Creating ImmunediveRsity/bin directory" "$?"

	    sudo cp bin/ImmunediveRsity /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ImmunediveRsity" "$?"
	    
	    sudo cp bin/ImmunediveRsity_notifier /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ImmunediveRsity_notifier" "$?"

	    sudo cp bin/acacia-1.52.b0.jar /opt/ImmunediveRsity/bin/.
	    verify_error "Moving /bin/acacia" "$?"

	    sudo cp bin/cutVDJ.pl /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/cutVDJ.pl" "$?"

	    sudo cp bin/parseKaks /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/parseKaks" "$?"

	    sudo cp bin/network_graph /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/network_graph" "$?"

	    sudo cp bin/complete_table /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/complete_table" "$?"

	    sudo cp bin/div_table /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/div_table" "$?"

	    sudo cp bin/usearc* /opt/ImmunediveRsity/bin/usearch
	    verify_error "Moving bin/usearch" "$?"
	    
	    # SUBSETS
	    sudo cp bin/ImmunediveRsity_subset /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ImmunediveRsity_subset" "$?"

	    sudo cp bin/parseKaks_subset /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/parseKaks_subset" "$?"

	    sudo cp bin/network_graph_subsets /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/network_graph_subset" "$?"

	    sudo cp bin/ng_index /opt/ImmunediveRsity/bin/.
	    verify_error "Moving bin/ng_index" "$?"


	    # Creating bin/blast directories
	    sudo mkdir /opt/ImmunediveRsity/bin/blast-2.2.22 
	    verify_error "Creating blast-2.2.22 directory" "$?"

	    sudo mkdir /opt/ImmunediveRsity/bin/blast-2.2.22/bin
	    verify_error "Creating bin/blast-2.2.22/bin directory" "$?"

	    sudo mkdir /opt/ImmunediveRsity/bin/blast-2.2.22/data 
	    verify_error "Creating bin/blast-2.2.22/data directory" "$?"

	    sudo cp bin/blast-2.2.22/bin/* /opt/ImmunediveRsity/bin/blast-2.2.22/bin/.
	    verify_error "Moving blast binaries" "$?"

	    sudo cp bin/blast-2.2.22/data/* /opt/ImmunediveRsity/bin/blast-2.2.22/data/.
	    verify_error "Moving blast data" "$?"

	    # creating ImmunediveRsity/data directory
	    sudo mkdir /opt/ImmunediveRsity/data
	    verify_error "Creating ImmunediveRsity/data" "$?"

	    sudo cp data/CDR3_IGHV_human.hmm /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/CDR3_IGHV_human.hmm" "$?"

	    sudo cp data/CDR3_mouse.hmm /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/CDR3_IGHV_mouse.hmm" "$?"

	    sudo cp data/blastfirsthit_q.pl /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/blastfirtshit_q.pl" "$?"

	    sudo cp data/gene_coordinates.txt /opt/ImmunediveRsity/data/.
	    verify_error "Moving data/gene_coordinates.txt" "$?"

	    # Creating data/LocusIgH_db
	    sudo mkdir /opt/ImmunediveRsity/data/LocusIgH_db
	    verify_error "Creating data/LocusIgH_db" "$?"

	    sudo cp data/LocusIgH_db/* /opt/ImmunediveRsity/data/LocusIgH_db/.
	    verify_error "Moving data/LocusIgH_db files" "$?"

	    # creating data/IGBLAST
	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST
	    verify_error "Creating data/IGBLAST directory" "$?"

	    sudo cp data/IGBLAST/ig* /opt/ImmunediveRsity/data/IGBLAST/.
	    verify_error "Moving igblast binaries" "$?"
	    
	    sudo cp data/IGBLAST/myseq /opt/ImmunediveRsity/data/IGBLAST/.
	    verify_error "moving IGBLAST/myseq" "$?"

	    # creating data/IGBLAST/database
	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST/database
	    verify_error "Creating data/IGBLAST/database" "$?"

	    sudo cp data/IGBLAST/database/* /opt/ImmunediveRsity/data/IGBLAST/database/.
	    verify_error "Moving IGBLAST/database" "$?"

	    # creating data/IGBLAST/functional_db
	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST/functional_db 
	    verify_error "Creating data/IGBLAST/functional_db" "$?"

	    sudo cp data/IGBLAST/functional_db/* /opt/ImmunediveRsity/data/IGBLAST/functional_db/.
	    verify_error "Moving IGBLAST/functional_db" "$?"

	    # creating IGBLAST/internal_data
	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST/internal_data
	    verify_error "Creating IGBLAST/internal_data" "$?"
	    
	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST/internal_data/human
	    verify_error "Creating IGBLAST/internal_data/human" "$?"

	    sudo cp data/IGBLAST/internal_data/human/* /opt/ImmunediveRsity/data/IGBLAST/internal_data/human/.
	    verify_error "Moving internal_data/human" "$?"

	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST/internal_data/mouse
	    verify_error "Creating internal_data" "$?"

	    sudo cp data/IGBLAST/internal_data/mouse/* /opt/ImmunediveRsity/data/IGBLAST/internal_data/mouse/.
	    verify_error "Moving internal_data/mouse" "$?"

	    # creating IGBLAST/optional_file
	    sudo mkdir /opt/ImmunediveRsity/data/IGBLAST/optional_file
	    verify_error "Creating IGBLAST/optional_file" "$?"

	    sudo cp data/IGBLAST/optional_file/* /opt/ImmunediveRsity/data/IGBLAST/optional_file/.
	    verify_error "Moving IGBLAST/optional_file" "$?"

	    # Creating ImmunediveRsity/ImmunediveRsity directories
	    sudo mkdir /opt/ImmunediveRsity/ImmunediveRsity
	    verify_error "Creating ImmunediveRsity/ImmunediveRsity" "$?"

	    sudo cp ImmunediveRsity/DESCRIPTION /opt/ImmunediveRsity/ImmunediveRsity/.
	    verify_error "Moving ImmunediveRsity/DESCRIPTION" "$?"

	    sudo cp ImmunediveRsity/NAMESPACE /opt/ImmunediveRsity/ImmunediveRsity/.
	    verify_error "Moving ImmunediveRsity/NAMESPACE" "$?"

	    # Creating ImmunediveRsity R directory
	    sudo mkdir /opt/ImmunediveRsity/ImmunediveRsity/R 
	    verify_error "Creating ImmunediveRsity/R directory" "$?"

	    sudo cp ImmunediveRsity/R/* /opt/ImmunediveRsity/ImmunediveRsity/R/.
	    verify_error "Moving R libraries" "$?"
	    
	    # Creating ImmunediveRsity/extdata
	    sudo mkdir /opt/ImmunediveRsity/ImmunediveRsity/extdata
	    verify_error "Creating ImmunediveRsity/extdata" "$?"
	    
	    sudo cp ImmunediveRsity/extdata/* /opt/ImmunediveRsity/ImmunediveRsity/extdata
	    verify_error "Moving ImmunediveRsity/extdata" "$?"

	    # Creating ImmunediveRsity/extras
	    sudo mkdir /opt/ImmunediveRsity/ImmunediveRsity/extras
	    verify_error "Creating ImmunediveRsity/extras" "$?"

	    sudo cp ImmunediveRsity/extras/* /opt/ImmunediveRsity/ImmunediveRsity/extras/.
	    verify_error "Moving ImmunediveRsity/extras" "$?"

	    # Creating ImmunediveRsity/man
	    sudo mkdir /opt/ImmunediveRsity/ImmunediveRsity/man
	    verify_error "Creating ImmunediveRsity/man" "$?"

	    sudo cp ImmunediveRsity/man/* /opt/ImmunediveRsity/ImmunediveRsity/man/.
	    verify_error "Moving ImmunediveRsity/man" "$?"

	    # Goto ImmunediveRsity directory
	    cd /opt/ImmunediveRsity

	    # Add executable permission
	    sudo chmod +x /opt/ImmunediveRsity/bin/ImmunediveRsity*
	    verify_error "Setting executable permission to ImmunediveRsity" "$?"

	    sudo chmod +x /opt/ImmunediveRsity/bin/*
	    verify_error "Setting executable permission to ImmunediveRsity/bin" "$?"

	    sudo chmod +x /opt/ImmunediveRsity/bin/blast-2.2.22/bin/*
	    verify_error "Setting executable permission to blast-2.2.22" "$?"

	    sudo chmod +x /opt/ImmunediveRsity/data/IGBLAST/igblastn 
	    verify_error "Setting executable permission to igblastn" "$?"

	    sudo chmod +x /opt/ImmunediveRsity/data/IGBLAST/igblastp 
	    verify_error "Setting executable permission to igblastp" "$?"
	    
	    ########################
	    # adding files to path #
	    ########################

	    # creating symlinks to /usr/bin
	    sudo ln -s /opt/ImmunediveRsity/bin/ImmunediveRsity /usr/bin/ImmunediveRsity
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity" "$?"

	    sudo ln -s /opt/ImmunediveRsity/bin/ImmunediveRsity_notifier /usr/bin/ImmunediveRsity_notifier
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity_notifier" "$?"

	    sudo ln -s /opt/ImmunediveRsity/bin/ImmunediveRsity_subset /usr/bin/ImmunediveRsity_subset
	    verify_error "Creating symbolic link to /usr/bin/ImmunediveRsity_subset" "$?"

	    sudo ln -s /opt/ImmunediveRsity/bin/blast-2.2.22/bin/blastall /usr/bin/blastall
	    verify_error "Creating symbolic link to /usr/bin/blastall" "$?" 

	    export PATH
	    
	    ###############
	    # configuring #
	    ###############
	    sys=$(verify_system)

	    if [ "$sys" = "Linux" ]; then
		sudo sed -i "s+\$/ = \"\# IGBLASTN 2.2.27\+\";+\$/ = \"\# IGBLASTN 2.2.26\+\";+g" /opt/ImmunediveRsity/data/IGBLAST/igparse.pl
		verify_error "Configuring igparse.pl" "$?"

		sudo sed -i "s/-auxiliary_data/-auxilary_data/g" /opt/ImmunediveRsity/ImmunediveRsity/R/runigblast.R
		verify_error "Configuring runigblast" "$?"
	    fi
	    
	    ##########################
	    # Installing R libraries #
	    ##########################

	    echo ""
	    sudo R CMD INSTALL ./ImmunediveRsity
	    echo ""
	    verify_error "Installing R libraries" "$?"
	    ;;
    esac
}

wellcome
verify_prev_installation
verify_dependencies
install
