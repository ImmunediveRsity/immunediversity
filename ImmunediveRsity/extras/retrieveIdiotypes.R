#  retrieveIdiotypes
#  
#  Copyright 2013-214
#
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#


retrieveIdiotypes <-
function(cg, output){
	# This function takes one clonal group identifier and searches in the groupsfullVJ.txt file its corresponding idiotypes and their different frequencies.
	# Usage: retrieveIdiotypes("HA7IYCV01.IGHV4-b.IGHJ5.4", "/partA/hugo/Dengue1/HA7IYCV01/")
	tabla<-read.table(paste(output, "groupsfullVJ.txt", sep=""))
	idiotypes<-as.vector(unique(tabla$V1[grep(cg, tabla$V1)]))
	frequencies<-sapply(idiotypes, function(x){sum(tabla$V1==x)})
	Idiotypes<-cbind(rev(sort(frequencies)), rev(sort(frequencies))/length(tabla$V1))
	colnames(Idiotypes)<-c("Raw Frequency", "Relative Frequency")
	return(Idiotypes)
}
