#  fastaCorrector.R
#  
#  Copyright 2013-214
#
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#


fastaCorrector<-function(groups, consensus, originalIDs, outfile){
	# This function takes a consensus file and returns each consensus repeated the times the consensus is present in the original fasta file.
	# USAGE: fastaCorrector("/partA/hugo/Influenza/Control/control1/groupsfullVJ.txt", "/partA/hugo/Influenza/Control/control1/consensusfullVJ.fa", "/partA/hugo/Influenza/Control/control1/VDJs.txt", "/partA/hugo/Influenza/Control/control1/correctedFasta.fa")
#	suppressMessages(library(ShortRead))
	consensus<-readFasta(consensus)
	groups<-read.table(groups)
	originalIDs<-read.table(originalIDs, header=TRUE, sep="\t")
	IDs<-BStringSet(originalIDs$read)	
	sapply(1:length(IDs), function(x){
		new<-ShortRead( sread=sread(consensus[which(as.character(id(consensus))==groups$V1[x])]), id=IDs[x] )
		writeFasta(new, outfile, mode="a")
	})
}

#groups<-"/partA/hugo/Influenza/PDM/pdm3/groupsfullVJ.txt"
#consensus<-"/partA/hugo/Influenza/PDM/pdm3/consensusfullVJ.fa"
#originalIDs<-"/partA/hugo/Influenza/PDM/pdm3/VDJs.txt"
#outfile<-"/partA/hugo/Influenza/PDM/pdm3/correctedFasta.fa"
#fastaCorrector(groups, consensus, originalIDs, outfile)
