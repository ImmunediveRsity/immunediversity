#  runigblast.R
#  
#  Copyright 2013-2014 
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


runigblast <-
    function(reads,
             Vdatabase="functional_db/human_gl_V",
             Ddatabase="functional_db/human_gl_D",
             Jdatabase="functional_db/human_gl_J",
             auxdata="optional_file/human_gl.aux",
             bin="/opt/ImmunediveRsity/data/IGBLAST/",
             perl="igparse.pl",
             proc=1,
             file=NULL,
             cdr3=FALSE,
             fasta="CDR3.fa",
             trimmedreads="onlyVDJ.fa",
             outpath=getwd())
{
    # This function uses igblast to obtain the V, D and J segments and their associated positions.
    # It returns a file with suggested VDJ and a data.frame with the proposed VDJ,
    # their positions in the read, the V-D and D-J junctions and whether it is in frame or not.
    # If no germline databases are specified it uses default which are for human.
    # If cdr3 is set to TRUE it extracts the CDR3 from the IGBLAST report and stores
    # the trimmed sequences in a FASTA file.
    # Also a file containing only the VDJ segments of the reads is generated.
    # It is very important to use the full path for the reads because otherwise the file
    # will be searched relative to the bin.

    if (!(file.exists(reads)))
        stop("File specified in 'reads' does not exist.")
    
    if (!(file.exists(bin)))
        stop("Path specified in 'bin' does not exist.")
    
    if (file.exists(trimmedreads))
        stop("'trimmedreads' must be a non existing file, delete it or set a new file name.")
    

    if (!(is.numeric(proc) & proc > 0)) {
        if (proc > detectCores()) {
            proc <- detectCores()
        }
    }
    
    if (!(is.logical(cdr3)))
        stop("'cdr3' must be TRUE or FALSE, TRUE if you want to use obtain the cdr3 from the IGBLAST report and FALSE if you don't want to obtain it from the report.")
    
    if (cdr3 == TRUE) {
        if (file.exists(fasta))
            stop("'fasta' must be a non existing file, delete it or set a new file name.")
    }
     
    # get VDJs.txt file, file with only VDJ segmets
    print("Using igblast to assign the V, D and J segments and alleles.")
    # Use igblast to identify the V, D and J segments.
    # The outfile is named igblast.out, it is required to be named that way as it is used
    # in later steps.

    ############################################################################
    #
    # Multi blast
    #

    start <- 1
    end <- 0
    
    set <- readDNAStringSet(reads)
    total <- length(set)
    
    step <- round(total / proc, 0)

    # split sequences according to the numbre of cores to use
    for (part in 1:proc) {
        end <- start + step
        
        no <- part
        if (no < 10) {
            no <- paste('0', part, sep='')
        }
        
        if (part == proc | end >= total){
            writeFasta(set[start:total], paste("sequences", no, sep='.'))
        } else {
            writeFasta(set[start:end], paste("sequences", no, sep='.'))
        }
        start <- end + 1
    }
    
    process <- list()
    for (i in 1:proc) {
        no <- i
        
        if (no < 10) {
            no <- paste('0', i, sep='')
        }

        if (is.na(auxdata)) {
            cmd <-  paste("cd ", bin, " \n",
                          " ./igblastn -germline_db_V ", Vdatabase,
                          " -germline_db_J ", Jdatabase,
                          " -germline_db_D ", Ddatabase,
                          " -domain_system imgt -query",
                          outpath,"/sequences.", no,
                          " -outfmt 7 -strand minus -num_threads ", proc,
                          " -out ", outpath, "/igblast.out.", no, sep="")
        } else {
            cmd <- paste("cd ", bin, " \n",
                         " ./igblastn -germline_db_V ", Vdatabase,
                         " -germline_db_J ", Jdatabase,
                         " -germline_db_D ", Ddatabase,
                         " -domain_system imgt -query ",
                         outpath,"/sequences.", no,
                         " -auxilary_data ", auxdata,
                         " -outfmt 7 -strand minus -num_threads ", proc,
                         " -out ", outpath, "/igblast.out.", no, sep="")
        }
        process[[i]] <- mcparallel(system(cmd))
    }
    mccollect(process, wait=TRUE)
        
    system("cat igblast.out.* > igblast.out")  # contcat igblast.out files
    system("rm sequences.* igblast.out.*", wait=TRUE)  # delete temp files
    
    #
    # End multi blast
    #
    ############################################################################

    # It uses a perl script named igparse.pl used to parse igblast's output.
    # It is included along with the package.
    # The output is named parsedigblast and this file is required in later steps.
    # The output contains the id, the used VDJ segments, the frame, the VD Junction,
    # the DJ Junction and the positions in the read of the VDJ segments.

    print("Parsing igblast output")
    system(paste("perl ", bin, perl, " igblast.out", sep=""), wait=TRUE)
    igblast <- read.table("parsedigblast", sep="\t", comment.char="")
    reads <- readFasta(reads)
        
    V <- D <- J <- NULL
    
    V[which(as.character(igblast[, 2]) == as.character(igblast[, 12]))] <-
        as.vector(igblast[which(as.character(igblast[, 2]) == as.character(igblast[, 12])), 2])
    
    V[is.na(V)] <- ""
    
    if (length(V) < length(reads)){
        V[(length(V) + 1):length(reads)] <- ""
    }
    
    D[which(as.character(igblast[, 3]) == as.character(igblast[, 15]))] <-
        as.vector(igblast[which(as.character(igblast[, 3]) == as.character(igblast[, 15])), 3])
    
    D[is.na(D)] <- ""
        
    if (length(D) < length(reads)){
        D[(length(D)+1):length(reads)] <- ""
    }
    
    J[which(as.character(igblast[, 4]) == as.character(igblast[, 18]))] <-
        as.vector(igblast[which(as.character(igblast[, 4]) == as.character(igblast[, 18])), 4])
    
    J[is.na(J)] <- ""
    
    if (length(J) < length(reads)){
        J[(length(J) + 1):length(reads)] <- ""
    }
    
    VDJ <- data.frame(read=igblast[, 1], V, D, J)
    
    if (!is.null(file)) {
        write.table(VDJ, file=file, quote=FALSE, col.names=TRUE,
                    row.names=FALSE, sep="\t")
    }
    
    VDJ <- data.frame(VDJ, igblast[, c(6:11, 13, 14, 16, 17, 19, 20)])
    names(VDJ) <- c("id", "V", "D", "J", "Frame", "Vcdr3", "V-D Junction",
                    "Dcdr3", "D-J Junction", "Jcdr3", "Vleft", "Vright", "Dleft",
                    "Dright", "Jleft", "Jright")
    
    if (cdr3 == TRUE) {
        cdr3 <- paste(VDJ[, 6], VDJ[, 7], VDJ[, 8],
                      VDJ[, 9], VDJ[, 10], sep="")
        cdr3 <- gsub("N\\/A", "", cdr3)
        cdr3 <- gsub("\\(", "", cdr3)
        cdr3 <- gsub("\\)", "", cdr3)
        
        cdr3[which(cdr3 < 1)] <- as.character(sread(reads[which(cdr3 < 1)]))
        
        new <- ShortRead(sread=DNAStringSet(cdr3), id=BStringSet(VDJ[, 1]))
        writeFasta(new, file=fasta)
    }
    
    left <- width(reads) - VDJ$Jright + 1
    right <- width(reads) - VDJ$Vleft + 1
        
    # Printing VDJ region
    writeFasta(reads, file=trimmedreads)
        
    if (trimmedreads == "Alleles_onlyVDJ.fa"){
        # get onlyV file to get KaKs
        # Position of the last nucleotide of the V region
        Vend <- width(reads) - VDJ$Vright + 1
            
        # Printing V region
        new <- ShortRead(sread=subseq(sread(reads), start=Vend, end=right), id=id(reads))
        writeFasta(new, file="WellSupportedCGs/onlyV.fa")
        
        print("Removing temporary files.")
        system("rm parsedigblast igblast.out", wait=TRUE)
    }        
    return(VDJ)
}
    
